Name:           libevent
Version:        2.1.12
Release:        15
Summary:        An event notification library

License:        BSD
URL:            http://libevent.org/
Source0:        https://github.com/libevent/libevent/releases/download/release-%{version}-stable/libevent-%{version}-stable.tar.gz

BuildRequires: gcc doxygen openssl-devel autoconf automake libtool

Patch0: libevent-nonettests.patch
Patch1: http-add-callback-to-allow-server-to-decline-and-the.patch
Patch2: add-testcases-for-event.c-apis.patch

# Temporary downstream change: revert a problematic upstream change
# until Transmission is fixed. Please drop the patch when the Transmission
# issue is fixed.
# https://github.com/transmission/transmission/issues/1437
Patch3: 0001-Revert-Fix-checking-return-value-of-the-evdns_base_r.patch
Patch6000: backport-ssl-do-not-trigger-EOF-if-some-data-had-been-successf.patch 
Patch6001: backport-http-eliminate-redundant-bev-fd-manipulating-and-cac.patch
Patch6002: backport-http-fix-fd-leak-on-fd-reset-by-using-bufferevent_re.patch
Patch6003: backport-bufferevent-introduce-bufferevent_replacefd-like-set.patch
Patch6004: backport-evutil-don-t-call-memset-before-memcpy.patch
Patch6005: 0002-Avoid-calling-read-2-on-eventfd-on-each-event-loop-w.patch  
Patch6006: backport-Fix-potential-Null-pointer-dereference-in-regress_fi.patch

Patch6007: backport-Fix-potential-Null-pointer-dereference-in-regress_thread.c.patch
Patch6008: backport-Fix-potential-Null-pointer-dereference-in-regress_buffer.c.patch
Patch6009: backport-Fix-potential-Null-pointer-dereference-in-regress_et.c.patch
Patch6010: backport-Fix-leak-in-evconnlistener_new_async.patch

Patch0004: 0004-fix-function-undeclared.patch

%description
Libevent additionally provides a sophisticated framework for buffered network IO, with support for sockets,
filters, rate-limiting, SSL, zero-copy file transmission, and IOCP.
Libevent includes support for several useful protocols, including DNS, HTTP, and a minimal RPC framewor.

%package devel
Summary: Development files for %{name}
Requires: %{name} = %{version}-%{release}

%description devel
This package contains the header files and libraries for developing
with %{name}.

%prep
%autosetup -n libevent-%{version}-stable -p1

%build
%configure --disable-dependency-tracking --disable-static
%make_build

%install
%make_install
rm -f %{buildroot}%{_libdir}/*.la

%check
# Test fail due to nameserver not running locally
# [error msg] Nameserver 127.0.0.1:38762 has failed: request timed out
# On some architects this error is ignored on others it is not
##%make_build check

%ldconfig_scriptlets

%files
%doc ChangeLog
%license LICENSE
%{_libdir}/libevent-2.1.so.*
%{_libdir}/libevent_core-2.1.so.*
%{_libdir}/libevent_extra-2.1.so.*
%{_libdir}/libevent_openssl-2.1.so.*
%{_libdir}/libevent_pthreads-2.1.so.*

%files devel
%{_includedir}/*.h
%dir %{_includedir}/event2
%{_includedir}/event2/*.h
%{_libdir}/libevent.so
%{_libdir}/libevent_core.so
%{_libdir}/libevent_extra.so
%{_libdir}/libevent_openssl.so
%{_libdir}/libevent_pthreads.so
%{_libdir}/pkgconfig/libevent.pc
%{_libdir}/pkgconfig/libevent_core.pc
%{_libdir}/pkgconfig/libevent_extra.pc
%{_libdir}/pkgconfig/libevent_openssl.pc
%{_libdir}/pkgconfig/libevent_pthreads.pc
%{_bindir}/event_rpcgen.*


%changelog
* Sun Oct 27 2024 zhangyaqi <zhangyaqi@kylinos.cn> - 2.1.12-15
- Fix leak in evconnlistener_new_async()

* Fri Aug 30 2024 yuanchao <1050706328@qq.com> - 2.1.12-14
- Fix function undeclared,incompatible pointer and parameter lack in 'add-testcases-for-event.c-apis.patch',support clang build

* Thu Aug 15 2024 zhangxingrong <zhangxingrong@uniontech.cn> - 2.1.12-13
- Fix potential Null pointer dereference in regress_thread.c
- Fix potential Null pointer dereference in regress_buffer.c
- Fix potential Null pointer dereference in regress_et.c

* Thu May 30 2024 baiguo <baiguo@kylinos.cn> - 2.1.12-12
- Fix potential Null pointer dereference in regress_finalize.c 

* Wed May 08 2024 baiguo <baiguo@kylinos.cn> - 2.1.12-11
- Avoid calling read(2) on eventfd on each event-loop wakeup

* Mon Apr 01 2024 shixuantong <shixuantong1@huawei.com> - 2.1.12-10
- evutil: don't call memset before memcpy

* Mon Sep 18 2023 shixuantong <shixuantong1@huawei.com> - 2.1.12-9
- eliminate redundant bev fd manipulating and caching

* Sat Jul 29 2023 shixuantong <shixuantong1@huawei.com> - 2.1.12-8
- ssl: do not trigger EOF if some data had been successfully read

* Thu Feb 16 2023 dillon chen <dillon.chen@gmail.com> - 2.1.12-7
- close make check

* Fri Jun 24 2022 dillon chen <dillon.chen@gmail.com> - 2.1.12-6
- add patch3

* Tue Nov 23 2021 Hu Bin <hubin571@huawei.com> - 2.1.12-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add testcases for event.c apis

* Wed Apr 21 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 2.1.12-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:use make macros to run check

* Mon Mar 29 2021 panxiaohe <panxiaohe@huawei.com> - 2.1.12-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add debuginfo package and make ELF files stripped
       remove redundant ABI compatibility library

* Thu Mar 18 2021 yang_zhuang_zhuang <yangzhuangzhuang1@huawei.com> - 2.1.12-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Optimize compilation time

* Thu Jul 30 2020 yang_zhuang_zhuang <yangzhuangzhuang1@huawei.com> - 2.1.12-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 2.1.12

* Wed Jul 1 2020 Liquor <lirui130@huawei.com> - 2.1.11-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix undefined-shift in EVUTIL_IS*_ helpers

* Mon Oct 28 2019 chengquan <chengquan3@huawei.com> - 2.1.11-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add global marco of debug_package

* Tue Aug 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.1.11-1
- Package init
